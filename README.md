# 介绍
mycat 自动解密程序，在 decrypt.xml 文件配置解密相关的信息，即可实现自动解密功能，配置方法跟 mycat 一致<br>
# 使用方式
[下载 v1.6.7.6-release](https://gitee.com/zrxjava/mycat-decrypt/releases/v1.6.7.6-release)<br>
cd bin<br>
mycat start <br>
解密执行完毕之后程序会自动退出
# 注意事项
执行解密程序之前先去 mycat 里的 server.xml 文件把 encrypt 改为 0，然后重启 mycat，再执行解密程序，避免解密后 mycat 又重新加密
