package io.mycat.config.model.encrypt;

import java.util.Map;

/**
 * @ClassName EncryptColumn
 * @Author zrx
 * @Date 2022/1/31 12:15
 */
public class EncryptTablePk {

	private String name;
	private String pk;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPk() {
		return pk;
	}

	public void setPk(String pk) {
		this.pk = pk;
	}
}

